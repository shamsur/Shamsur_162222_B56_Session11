<?php

// Define fucntion Variable;
define("BITM","BASIS Institute of Technology & Management");
echo constant("BITM")."<br><br>";

// Magic Constant : _LINE_
echo "Magic Constant LINE was in line#".__LINE__;
echo "<br> <br>";

// Magic Constant : _FILE_
echo "This File was in path : ".__FILE__;
echo "<br> <br>";

// Magic Constant : __DIR__
echo __DIR__;
echo "<br><br>";
// Magic Constant __FUNCTION__
function myName(){
    echo __FUNCTION__;
    echo"<br><br>";
}
myName();

// Expression
$a = 20;
$s = 10;

echo "Addition of this two variable is : ".($a+$s)."<br>";
echo "Subtraction of this two variable is : ".($a-$s)."<br>";
echo "Multiplication of this two variable is : ".($a*$s)."<br>";
echo "Division of this two variable is : ".($a/$s)."<br>";
echo "Remainder of this two variable is : ".($a%$s)."<br>";
echo "Exponentiation of this two variable is : ".($a**$s)."<br><br><br>";

// Assignment Operator

$x = 15;
$y = 5;
echo "If  \$x = 15  and " . " \$y = 5,"."<br>";
echo "\$x + \$y = ".($x+$y)."<br>";
echo "\$x - \$y = ".($x-$y)."<br>";
echo "\$x * \$y = ".($x*$y)."<br>";
echo "\$x / \$y = ".($x/$y)."<br>";
echo "\$x % \$y = ".($x%$y)."<br>";
echo "\$x ^ \$y = ".($x**$y)." (exponention) <br><br>";


// Comparison Operators

echo "Comparison Operators....<br>";
echo "\$x == \$y : ";
echo ($x==$y)."<br>";
echo "\$x != \$y : ";
echo ($x!=$y)."<br>";
echo "\$x <> \$y : ";
echo ($x<>$y)."<br>";
echo "\$x === \$y : ";
echo ($x===$y)."<br>";
echo "\$x !== \$y : ";
echo ($x!==$y)."<br>";
echo "\$x > \$y : ";
echo ($x>$y)."<br>";
echo "\$x >= \$y : ";
echo ($x>=$y)."<br>";
echo "\$x < \$y : ";
echo ($x<$y)."<br>";
echo "\$x <= \$y : ";
echo ($x<=$y)."<br><br>";

// String Opearator .(dot)

echo "String Opearator .(dot is used for concatination) <br>";
$str1 = "Shamsur ";
$str2 = "Rahaman";
echo $str1 . $str2."<br>";

// Array  Operator
$arr1 = array("first" => "Shamsur", "second" => "Rasel", "third" => "Sakil");
$arr2 = array("forth" => "Rahaman", "fifth" => "Hasan", "sixth" => "Rahman");

$arr3 = $arr1+$arr2;
echo "<pre>";
print_r($arr3);
echo "</pre>";
echo "<br>";
echo "\$arr1==\$arr2 :<pre> ";
print_r($arr1==$arr2);
echo "</pre>";
echo "<br>";

// Addslashes function
echo "addslashes function<br>";
$str3 = "I'm Shamsur";
echo addslashes($str3);
echo "<br><br>";























